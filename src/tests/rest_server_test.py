import pytest
from werkzeug import exceptions
from src.simple_rest_server.rest_server import RestServer


@pytest.fixture(scope='session')
def server():
    server = RestServer()
    server.start()
    yield server
    server.stop()


@pytest.fixture(scope='session')
def client(server):
    with server.app.test_client() as client:
        with server.app.app_context():
            return client


def test_mock_with_json_serializable(server, client):
    # Arrange
    url = "/json"
    payload = dict(hello='poop')
    server.add_jsonify_response(url, payload)

    # Act
    response = client.get(url)

    # Assert
    assert 200 == response.status_code
    assert payload['hello'] == response.json["hello"]


def test_mock_with_callback(server, client):
    # Arrange
    payload = "poop"
    url = "/callback"
    server.add_callback_response(url, lambda : payload)

    # Act
    response = client.get(url)

    # Assert
    assert 200 == response.status_code
    assert payload == response.data.decode()


def test_mock_bad_request(server, client):
    # Arrange
    error_string = 'poop request!'
    url = "/not_found"
    NotFound = exceptions.NotFound.code
    server.set_error_handler_callback(error_string, NotFound)

    # Act
    response = client.get(url)

    # Assert
    assert NotFound == response.status_code
    assert error_string == response.data.decode()


def test_get_request(server, client):
    # Arrange
    def payload():
        req = server.get_request()
        return "poop" + req.endpoint
    url = "/get_request"
    server.add_callback_response(url, payload)

    # Act
    response = client.get(url)

    # Assert
    assert 200 == response.status_code
    assert "poop" + url == response.data.decode()